#!/bin/python3

from bs4 import BeautifulSoup
from requests import get
from requests.exceptions import RequestException
from urllib.parse import urlparse, urljoin
from functools import reduce
from uuid import uuid4
from configparser import ConfigParser, ParsingError, DuplicateOptionError

import csv
import json
import re
import os
import sys
import logging
import argparse
import sqlite

from private import MAPQUEST_KEY, LOCATIONIQ_KEY

PROG_NAME = 'World Radio Scrapper'
PROG_VERSION = 1.1

# URL_NOMINATIM = 'https://nominatim.openstreetmap.org'
URL_NOMINATIM = f'https://eu1.locationiq.com/v1/search.php?key={LOCATIONIQ_KEY}'
# URL_NOMINATIM = f'http://open.mapquestapi.com/nominatim/v1/search.php?key={MAPQUEST_KEY}'

URL_REVERSE_NOMINATIM = f'http://open.mapquestapi.com/nominatim/v1/reverse.php?key={MAPQUEST_KEY}'
# URL_REVERSE_NOMINATIM = f'https://eu1.locationiq.com/v1/reverse.php?key={LOCATIONIQ_KEY}'

DATA_DIR = './data'
LOGS_DIR = './logs'

CITIES_CSV = './assets/worldradiomap_cities.csv'
COUNTRIES_CSV = './assets/country_codes.csv'

QUERY_CREATE_PLACES = '''CREATE TABLE IF NOT EXISTS places
                         (place_id INT PRIMARY KEY,
                          place_name TEXT NOT NULL,
                          place_type TEXT NOT NULL,
                          place_country TEXT NOT NULL,
                          place_country_code TEXT NOT NULL,
                          place_lng REAL, place_lat REAL)'''
QUERY_CREATE_CITIES = '''CREATE TABLE IF NOT EXISTS cities
                         (city_id TEXT PRIMARY KEY,
                          city_name TEXT NOT NULL,
                          city_href TEXT NOT NULL,
                          city_place INT NOT NULL,
                          FOREIGN KEY(city_place) REFERENCES places(place_id))'''
QUERY_CREATE_ANTENNAS = '''CREATE TABLE IF NOT EXISTS antennas
                           (antenna_id TEXT PRIMARY KEY,
                            antenna_name TEXT NOT NULL,
                            antenna_place INT NOT NULL,
                            antanna_city TEXT NOT NULL,
                            FOREIGN KEY(antenna_place) REFERENCES places(place_id),
                            FOREIGN KEY(antanna_city) REFERENCES cities(city_id))'''
QUERY_CREATE_STREAMS = '''CREATE TABLE IF NOT EXISTS streams
                          (stream_id TEXT PRIMARY KEY,
                           stream_href TEXT NOT NULL UNIQUE,
                           stream_type TEXT,
                           stream_https INTEGER)'''
QUERY_CREATE_RADIOS = '''CREATE TABLE IF NOT EXISTS radios
                         (radio_id TEXT PRIMARY KEY,
                          radio_city TEXT NOT NULL,
                          radio_name TEXT NOT NULL,
                          radio_href TEXT NOT NULL,
                          radio_band TEXT,
                          radio_freq REAL,
                          radio_emitter TEXT,
                          radio_stream TEXT,
                          FOREIGN KEY(radio_stream) REFERENCES streams(stream_id),
                          FOREIGN KEY(radio_city) REFERENCES cities(city_id))'''

MIME_PLAYLIST = ['audio/x-mpegurl', 'audio/mpegurl', 'application/mpegurl', 'application/x-mpegurl',
                 'audio/scpls', 'audio/x-scpls', 'application/pls+xml', 'application/xspf+xml',
                 'application/vnd.apple.mpegurl', 'application/vnd.ms-wpl', 'application/x-winamp-playlist']

GEO_TYPES = ['city', 'town', 'village', 'suburb', 'atoll', 'island', 'administrative']
GEO_KEYS = ['display_name', 'place_id', 'type', 'address', 'lon', 'lat']


def _request(url, timeout=5):
    try:
        res = get(url, stream=True, timeout=timeout)
        ret_type = None
        if res.status_code == 200:
            if res.headers and 'Content-Type' in res.headers:
                ret_type = res.headers['Content-Type'].lower()

        return ret_type, res

    except RequestException:
        return None, None


def _geocode(country_code, query, locale, geo_types=GEO_TYPES, geo_keys=GEO_KEYS):
    def _filter_type(data, geo_type):
        i = 0
        while i < len(data):
            if data[i]['type'] == geo_type:
                ret = {k: data[i][k] for k in GEO_KEYS}
                ret['lng'] = ret['lon']
                del ret['lon']
                return ret
            i += 1

        return None

    p = f'format=json&accept-language={locale}&addressdetails=1&dedupe=1&countrycodes={country_code}&q={query}'
    # r = f'{URL_NOMINATIM}/search/?{p}&'
    r = f'{URL_NOMINATIM}&{p}'
    res = _request(r)[1]

    if res is None or res.status_code != 200:
        return None

    data = res.json()

    for gt in geo_types:
        ret = _filter_type(data, gt)
        if ret is not None:
            return ret

    print(data)
    return None


def _reverse_geocode(lat, lng, locale):
    p = f'format=json&accept-language={locale}&addressdetails=1&lat={lat}&lon={lng}'
    # r = f'{URL_NOMINATIM}/reverse/?{p}'
    r = f'{URL_REVERSE_NOMINATIM}&{p}'
    res = _request(r)[1]

    if res is None or res.status_code != 200:
        return None

    data = res.json()
    if 'address' not in data:
        return None

    ret = {k: data[k] for k in ['place_id', 'lon', 'lat', 'address']}
    ret['lng'] = ret['lon']
    del ret['lon']

    return ret


def _country_code(cc):
    if cc == 'uk':
        cc = 'gb'
    elif cc in ['lnr', 'dnr']:  # Luhansk, eastern Ukraine, donetsk, russian Ukraine...
        cc = 'ua'
    elif cc == 'krd':           # Erbile, kurdish Iraq...
        cc = 'iq'
    elif cc == 'kk':            # turkish, northern Cyprus...
        cc = 'cy'
    elif cc == 'ac':            # Ascencion Island -> Saint Helena
        cc = 'sh'
    elif cc == 'sml':           # Somalia
        cc = 'so'
    elif cc == 'ks':            # Kosovo -> Serbia
        cc = 'rs'
    elif cc == 'pmr':           # Rîbnița -> Moldova
        cc = 'md'
    elif cc == 'nkr':           # Stepanakert,  Republic of Artsakh -> Azerbaijan
        cc = 'az'
    elif cc in ['ab', 'os']:    # Abkhazia, South Ossetia -> Georgia
        cc = 'ge'
    elif cc in ['mq', 're', 'gf', 'gp', 'nc', 'pf', 'pm']:  # Oversea territories -> France
        cc = 'fr'
    elif cc in ['hk', 'mo']:    # Macau, Hong Kong -> China
        cc = 'cn'
    elif cc in ['vi', 'gu', 'as', 'mp', 'pr']:  # Oversea territories -> US
        cc = 'us'
    elif cc == 'ax':            # Aland Islands -> Finland
        cc = 'fi'
    elif cc in ['aw', 'cw']:    # Oversea territories -> Netherlands
        cc = 'nl'

    return cc


def _city_name(cn):
    if cn == 'Aqtaý':
        cn = 'Aktau Mangystau'
    elif cn == 'Atyraý':
        cn = 'Atyrau'
    elif cn == 'Bobruysk':
        cn = 'Babruysk'
    elif cn == 'Cd. de Guatemala':
        cn = 'Guatemala City'
    elif cn == 'Cd. de México':
        cn = 'Mexico City'
    elif cn == 'Cd. de Panamá':
        cn = 'Panama City'
    elif cn == 'Evpatoria':
        cn = 'Yevpatoria'
    elif cn == 'Hagåtña':
        cn = 'Hagåtña Municipality'
    elif cn == 'Innsburck':
        cn = 'Innsbruck'
    elif cn == 'Nur-Sultan':
        cn = 'Astana'
    elif cn == 'Petropavl':
        cn = 'Petropavlovsk'
    elif cn == 'Prishtina':
        cn = 'Pristina'
    elif cn == 'Qaraǵandy':
        cn = 'Karaganda'
    elif cn == 'Saint-Pierre, PM':
        cn = 'Saint-Pierre Miquelon'
    elif cn == 'San Juan':
        cn = 'San Juan, Puerto Rico'
    elif cn == 'Bangor, ME':
        cn = 'Bangor Maine'
    elif cn == 'Portland, ME':
        cn = 'Portland, Maine'
    elif cn == 'Monterey, CA':
        cn = 'Monterey, California'
    elif cn == 'San Francisco, CA':
        cn = 'San Francisco, California'
    elif cn == 'Santa Barbara, CA':
        cn = 'Santa Barbara, California'
    elif cn == 'Topeka, KS':
        cn = 'Topeka, Kansas'
    elif cn == 'St. John\'s, NL':
        cn = 'St. John\'s'
    elif cn == 'Kuzey Lefkoşa':
        cn = 'Lefkoşa'
    elif cn == 'Vaticana':
        cn = 'Vatican City'

    return cn


def _location(url):
    location = re.search(r'q=(-*\d+?\.\d+),\s*(-*\d+?\.\d+)', url)

    def _coordinate(coordinate):
        wrong_coordinate = re.search(r'(-{2,})', coordinate)

        if wrong_coordinate:
            return coordinate[wrong_coordinate.span()[1] - 1:]

        return coordinate

    return _coordinate(location.group(1)), _coordinate(location.group(2))


def _urls_find(text):
    urls = re.findall(r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\), ]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', text)
    return urls


def _url_clean(url):
    if '?' in url:
        url = url[:url.index('?')]

    if url.endswith('mp3.m3u'):
        url = url[:-4]

    if url.startswith('hhttp'):
        url = url[1:]

    return url


def _stream_parse(href):
    stream_href = _url_clean(href)
    stream_type, stream_res = _request(stream_href)

    if stream_type and stream_type in MIME_PLAYLIST:
        if 'pls' in stream_type:
            pls = ConfigParser()

            try:
                pls.read_string(stream_res.text)
                if 'playlist' in pls:
                    for sec in pls['playlist']:
                        if sec.startswith('file'):
                            stream_href = pls['playlist'][sec]
                            break
            except (ParsingError, DuplicateOptionError):
                lines = [l for l in stream_res.text.splitlines() if l.startswith('File')]
                if len(lines) > 0:
                    stream_href = lines[0].split('=').pop()

        elif 'mpegurl' in stream_type:
            stream_urls = _urls_find(stream_res.text)
            if len(stream_urls) > 0:
                stream_href = _url_clean(stream_urls[0])

        stream_type = _request(stream_href)[0]

    return stream_href, stream_type


def cmd_siphon(data_path, locale, log):
    if not os.path.exists(data_path):
        os.makedirs(data_path)

    country_codes_file = open(COUNTRIES_CSV)
    country_codes = csv.reader(country_codes_file)

    radiomap_cities_file = open(CITIES_CSV)
    radiomap_cities = csv.reader(radiomap_cities_file)

    city_total = sum(1 for line in radiomap_cities_file)
    city_count = 0
    radio_count = 0

    country_data = []
    radiomap_cities_file.seek(0)
    for city in radiomap_cities:
        url_raw = city[1]
        url_comp = urlparse(url_raw)
        url_parts = url_comp.path.split('/')

        city_name = _city_name(city[0])
        city_id = url_parts[-1][:-4]
        country_code = _country_code(url_parts[1])
        if 'us-' in country_code:
            city_id = f'{country_code.split("-")[1]}_{city_id}'
            country_code = 'us'

        country_codes_file.seek(0)
        country_code, country_name = next(cc for cc in country_codes if cc[0].lower() == country_code)
        if country_code not in [item['country']['code'] for item in country_data]:
            country_data.append({'country': {'name': country_name, 'code': country_code}, 'cities': []})

        country_path = f'{data_path}/{country_code.lower()}'

        city_path = f'{country_path}/{city_id}.json'
        if os.path.exists(city_path):
            city_count += 1
            continue

        if log:
            log_info = f'Scrapping {city_id} ({city_count+1}/{city_total})'
            print(log_info)
            logging.info(log_info)

        if city_name in ['Yevpatoria', 'Kerch', 'Sevastopol', 'Simferopol', 'Yalta']:
            country_code = 'UA'
        elif city_name == 'Pristina':
            country_code = 'XK'
        elif city_name == 'Tegucigalpa':
            country_code = 'HN'

        city_data = dict()
        city_place = _geocode(country_code, city_name, locale)
        if not city_place:
            log_info = f'Failed at geocoding {country_code}, {city_name}'
            print(log_info)
            logging.info(log_info)
            break

        city_data['origin'] = {'id': city_id, 'name': city_name, 'href': url_raw}
        city_data['place'] = city_place
        city_data['radios'] = list()
        city_data['antennas'] = list()

        dom = BeautifulSoup(_request(url_raw)[1].content, 'html.parser')

        antenna_data = dom.find_all('div', class_=re.compile(r'ta\d{1}'))
        if antenna_data:
            for antenna in antenna_data:
                antenna_link = antenna.find('a', href=re.compile(r'maps.google.com'))
                if not antenna_link:
                    continue

                antenna_name = antenna.find('h2').text.strip()
                antenna_lat, antenna_lng = _location(antenna_link['href'])

                antenna_place = _reverse_geocode(antenna_lat, antenna_lng, locale)
                if not antenna_place:
                    if log:
                        log_info = f'Failed at reverse geocoding antenna {antenna_name}'
                        print(log_info)
                        logging.warning(log_info)
                    break

                city_data['antennas'].append({'name': antenna_name, 'lat': antenna_lat, 'lng': antenna_lng,
                                              'place': antenna_place})

        if dom.find('div', id='main') is None:
            if log:
                log_info = f'Unable to find data for {city_id} ({url_raw})'
                print(log_info)
                logging.warning(log_info)
            city_count += 1
            continue

        radio_table = dom.find('table', class_='fix').find_all('tr')
        band_type = ''
        for row in radio_table[1:]:
            cols = row.find_all('td')
            if (len(cols) < 3):
                continue

            band_data = cols[0].text.strip()
            radio_data = cols[1].find_all('a')
            emitter_data = cols[2].text.strip()

            band_freq = re.match(r'^(\d+?\.\d+)', band_data)
            if not band_freq:
                band_freq = re.match(r'^(\d{2,})', band_data)

                for radio in radio_data:
                    is_tv = 'ТВ канал' in band_type
                    if is_tv:
                        continue

                    radio_url = radio['href']
                    if not re.search(r'\/play\/[\w|-]+.htm', radio_url):
                        continue

                    if 'http' not in radio_url:
                        radio_url = urljoin(url_raw, radio_url)

                    is_predavatel = 'predavatel' in url_raw
                    if is_predavatel:
                        if 'ТВ канал' in band_type:  # it's a TV
                            continue

                    radio_name = radio.text.strip()
                    if radio_name == '' and is_predavatel:
                        radio_name = radio_data[0].text.strip()

                    city_data['radios'].append({'name': radio_name, 'href': radio_url,
                                                'band': band_type, 'frequency': band_freq, 'emitter': emitter_data})

                    radio_count += 1
            else:
                band_type = band_data

        if not os.path.exists(country_path):
            os.mkdir(country_path)

        with open(city_path, 'w+') as output_file:
            json.dump(city_data, output_file)

        city_count += 1

    print(f'\nDone!\nScrapped {radio_count} radios in {city_count} cities.')


def cmd_print(db_file, data_dir, log):
    def _red(content):
        return f'\033[91m{content}\033[00m'

    def _yellow(content):
        return f'\033[93m{content}\033[00m'

    def _green(content):
        return f'\033[92m{content}\033[00m'

    def _cyan(content):
        return f'\033[96m{content}\033[00m'

    radiomap_cities_file = open(CITIES_CSV)
    radiomap_cities = csv.reader(radiomap_cities_file)

    city_total = sum(1 for line in radiomap_cities_file)
    radiomap_cities_file.seek(0)
    print(f'-> CSV source file {_cyan(CITIES_CSV)} lists {_green(city_total)} cities.')

    city_urls = {}
    for city in radiomap_cities:
        city_name = city[0]
        city_url = city[1]

        if city_url in city_urls:
            city_urls[city_url].append(city_name)
        else:
            city_urls[city_url] = [city_name]

    city_dups = {k: city_urls[k] for k in city_urls.keys() if len(city_urls[k]) > 1}
    radiomap_cities_file.seek(0)

    if len(city_dups) > 0:
        print(_yellow('!! Some cities points to the same url.'))
        for city_url in city_dups.keys():
            city_names = reduce(lambda x, y: f'{x}, {y}', city_dups[city_url])
            print(f'   {_cyan(city_url)} -> {_green(city_names)}')

    if log_file:
        with open(log_file) as logs:
            warnings = []
            for line in logs:
                log = line.strip()
                if 'Unable to find data for' in log:
                    log_data = re.search(r'data for (\w+) \((.+)\)', log)
                    warnings.append(log_data.groups())

            if len(warnings) > 0:
                print(f'\n-> There are {_yellow(len(warnings))} warnings from logs {log_file}')
                print(_yellow('!! Unable to find radios for:'))
                for city, url in warnings:
                    print(f'   {_green(city)} -> {_cyan(url)}')

    if os.path.exists(db_file):
        db = sqlite.Db(db_file)

        city_count = db.count('cities')
        antenna_count = db.count('antennas')
        radios = db.fetch_all('radios')

        radio_distinct = db.fetch_distinct('radios', ['radio_href'])
        antenna_distinct = db.fetch_distinct('antennas', ['antenna_name'])
        streams = db.fetch_all('streams')

        streams_typed_count = sum(1 for stream in streams if stream[3])
        radio_streamed_count = sum(1 for radio in radios if radio[6])

        print(f'\n-> SQLite database {_cyan(db_file)} lists {_green(city_count)} cities.')
        print(f'   {_cyan("radios")}   -> {_green(len(radios))} rows ({_green(len(radio_distinct))} point to distinct hrefs, {_green(radio_streamed_count)} to streams);')
        print(f'   {_cyan("antennas")} -> {_green(antenna_count)} rows ({_green(len(antenna_distinct))} are named differently).')
        print(f'   {_cyan("streams")}  -> {_green(len(streams))} rows ({_green(streams_typed_count)} present a valid audio source).')

    radio_count = 0
    country_count = 0
    city_count = 0
    antenna_count = 0
    nogeocodes = {}
    noaddresses = {}

    for dirpath, _dirnames, filenames in os.walk(data_dir):
        if dirpath is data_dir:
            continue

        country_count += 1
        city_count += len(filenames)

        if len(filenames) == 0:
            os.removedirs(dirpath)
            continue

        for filename in filenames:
            with open(f'{dirpath}/{filename}') as json_file:
                city_data = json.load(json_file)
                radio_count += len(city_data['radios'])
                no_geocode = True

                if 'antennas' in city_data:
                    no_geocode = False
                    antenna_count += len(city_data['antennas'])

                if 'place' not in city_data:
                    country_code = dirpath.split('/').pop()
                    city_name = city_data['origin']['name']

                    if no_geocode:
                        if country_code in nogeocodes:
                            nogeocodes[country_code].append(city_name)
                        else:
                            nogeocodes[country_code] = [city_name]
                    else:
                        noaddress = (city_name, f'{dirpath}/{filename}')
                        if country_code in noaddresses:
                            noaddresses[country_code].append(noaddress)
                        else:
                            noaddresses[country_code] = [noaddress]

    print('\n-> Data dir lists {} radios spread over {} countries ({} antennas in {} cities).'
          .format(_green(radio_count), _green(country_count), antenna_count, city_count))

    if len(nogeocodes) > 0:
        print(_yellow('!! Some cities provide no geolocations data.'))
        for country_code in nogeocodes.keys():
            city_names = reduce(lambda x, y: f'{x}, {y}', nogeocodes[country_code])
            print(f'   {_cyan(country_code)} -> {_green(city_names)}')

    if len(noaddresses) > 0:
        print(_yellow('!! Some cities provide no addresses.'))
        for country_code in noaddresses.keys():
            city_names = reduce(lambda x, y: f'{x}, {y}', [noaddress[0] for noaddress in noaddresses[country_code]])
            print(f'   {_cyan(country_code)} -> {_green(city_names)}')

        rm = input(f'\nDo you want to {_red("erase")} them? (y/n)\n')
        if rm == 'y' or rm == 'yes':
            rm_count = 0
            for country_code in noaddresses.keys():
                for noaddress in noaddresses[country_code]:
                    os.remove(noaddress[1])
                    rm_count += 1

            print(f'Removed {rm_count} files.')


def cmd_insert(db_file, data_dir, start_index, log):
    db = sqlite.Db(db_file, [QUERY_CREATE_PLACES, QUERY_CREATE_CITIES,  QUERY_CREATE_ANTENNAS,
                             QUERY_CREATE_STREAMS, QUERY_CREATE_RADIOS])

    def _city_values(origin, place_id):
        return [uuid4().hex, origin['name'], origin['href'], place_id]

    def _place_values(place, place_id):
        try:
            t = place['type']
        except KeyError:
            t = None

        address = place['address']
        name = ''

        if t != 'administrative' and t in address:
            name = address[t]
        else:
            t = [*address][0]
            name = address[t]

        country = address['country']
        country_code = address['country_code']
        lng = float(place['lng'])
        lat = float(place['lat'])

        return [place_id, name, t, country, country_code, lng, lat]

    def _radio_values(radio, city_id):
        ret = [uuid4().hex, city_id]
        ret.extend([radio[k] for k in radio.keys()])
        ret.append(None)

        return ret

    counter = 0
    for dirpath, _dirnames, filenames in os.walk(data_dir):
        if dirpath is data_dir:
            continue

        for filename in filenames:
            if counter < start_index:
                counter += 1
                continue

            counter += 1
            with open(f'{dirpath}/{filename}') as json_file:

                city = json.load(json_file)
                place = city['place']

                place_id = int(place['place_id'])
                place_record = db.fetch_one('places', 'place_id', place_id)
                if place_record:
                    if log:
                        log_info = f'Place "{place_record[1]}" (id: {place_record[0]}) already exists.'
                        print(log_info)
                        logging.info(log_info)
                    continue
                else:
                    place_values = _place_values(place, place_id)
                    db.add_one('places', place_values)

                    city_values = _city_values(city['origin'], place_id)
                    db.add_one('cities', city_values)

                if 'antennas' in city:
                    city_id = city_values[0]

                    for antenna in city['antennas']:
                        if 'place' not in antenna:
                            if log:
                                log_info = f'Antenna {antenna["name"]} misses geocoded place'
                                print(log_info)
                                logging.warning(log_info)
                            continue

                        place = antenna['place']
                        place_id = int(place['place_id'])
                        antenna_record = db.fetch_one('antennas', 'antenna_place', place_id)
                        if antenna_record:
                            if log:
                                log_info = f'Antenna {antenna_record[1]} already exists.'
                                print(log_info)
                                logging.info(log_info)
                            continue

                        place_values = _place_values(place, place_id)
                        db.add_one('places', place_values)

                        antenna_values = [uuid4().hex, antenna['name'], place_id, city_id]
                        db.add_one('antennas', antenna_values)

                radio_count = 0
                if 'radios' in city:
                    for radio in city['radios']:
                        radio_records = db.find('radios', 'radio_id',
                                                (None, 'radio_href', '=', radio['href']),
                                                ('AND', 'radio_city', '=', city_id))
                        if len(radio_records) > 0:
                            continue

                        radio_values = _radio_values(radio, city_id)
                        db.add_one('radios', radio_values)
                        radio_count += 1

            print(f'{counter:<5} - {city_values[1]} processed with {radio_count} radios')


def cmd_test(db_file, data_dir):
    # url = 'http://www.predavatel.com/mk/7/ohrid'
    # url = 'http://worldradiomap.com/us-wi/green-bay'

    def _parse_radios(url):
        radios = []

        dom = BeautifulSoup(_request(url)[1].content, 'html.parser')
        radio_table = dom.find('table', class_='fix').find_all('tr')
        band_type = ''

        for row in radio_table[1:]:
            cols = row.find_all('td')
            if (len(cols) < 3):
                continue

            band_data = cols[0].text.strip()
            radio_data = cols[1].find_all('a')
            emitter_data = cols[2].text.strip()

            band_freq = re.match(r'^(\d+?\.\d+)', band_data)
            if not band_freq:
                band_freq = re.match(r'^(\d{2,})', band_data)

            if band_freq:
                band_freq = band_freq.group(1)

                for radio in radio_data:
                    is_tv = 'ТВ канал' in band_type
                    if is_tv:
                        continue

                    radio_url = radio['href']
                    if not re.search(r'\/play\/[\w|-]+.htm', radio_url):
                        continue

                    if 'http' not in radio_url:
                        radio_url = urljoin(url, radio_url)

                    is_predavatel = 'predavatel' in url
                    if is_predavatel:
                        is_tv = 'ТВ канал' in band_type
                        if is_tv:
                            continue

                    radio_name = radio.text.strip()
                    if radio_name == '' and is_predavatel:
                        radio_name = radio_data[0].text.strip()

                    radios.append({'name': radio_name, 'href': radio_url,
                                   'band': band_type, 'frequency': band_freq, 'emitter': emitter_data})

            else:
                band_type = band_data

        return radios

    radio_count = 0
    for dirpath, _dirnames, filenames in os.walk(data_dir):
        if dirpath is data_dir:
            continue
        for filename in filenames:

            with open(f'{dirpath}/{filename}') as json_file:
                city_data = json.load(json_file)

            city_href = city_data['origin']['href']

            if 'predavatel' in city_href:
                city_data['radios'] = _parse_radios(city_href)
                with open(f'{dirpath}/{filename}', 'w+') as json_file:
                    json.dump(city_data, json_file)
                    radio_count += len(city_data["radios"])

    print(radio_count)


def cmd_validate(db_file, start_index, log):
    def _has_https(url):
        url_comp = urlparse(url)

        if url_comp.scheme == 'http':
            stream_type = _stream_parse(f'https{url[4:]}')[1]
            if stream_type is not None:
                return 1
            else:
                return 0
        elif url_comp.scheme == 'https':
            return 1

    def _audio_src(element):
        if not element:
            return None
        elif 'src' in element:
            return element['src']

        source = element.find('source')
        if source and 'src' in source:
            return source['src']

        return None

    def _sanitize_type(mime_type):
        mime_group = mime_type.split(';')
        if len(mime_group) > 1 and 'charset' in mime_group[1]:
            mime_type = mime_group[0].strip()

        mime_group = mime_type.split(',')
        if len(mime_group) > 1:
            mime_type = mime_group[0].strip()

        return mime_type

    db = sqlite.Db(db_file)
    radios = db.fetch_group('radios', 'radio_href')
    updated_ids = set()

    if log:
        logging.info('-- START OF VALIDATION LOG --')


    counter = 0
    for radio in radios:
        if counter < start_index-1:
            counter += 1
            continue

        counter += 1
        radio_id = radio[0]
        if radio_id in updated_ids:
            continue

        log_info = f'{counter:<5} - {radio[2]}::'

        radio_href = radio[3]
        player = _request(radio_href)[1].content
        if not player:
            if log:
                log_info += f' no player for {radio_href}'
                print(log_info)
                logging.warning(log_info)
            continue

        dom_root = BeautifulSoup(player, 'html.parser')
        dom_player = dom_root.find('audio')
        stream_href = _audio_src(dom_player)
        stream_type = None

        if not stream_href:
            dom_playlist = dom_root.find('div', id='playlist')
            if dom_playlist:
                stream_href = dom_playlist.find('a')['href']

        if stream_href:
            if stream_href.startswith('hhttp'):
                stream_href = stream_href[1:]

            if u'\u200e' in stream_href:
                stream_href = stream_href.replace(u'\u200e', '')

            stream_href, stream_type = _stream_parse(stream_href)

        if not stream_href:
            if log:
                log_info += f' no source for {radio_href}'
                print(log_info)
                logging.warning(log_info)
            continue

        stream_record = db.find('streams', 'stream_id', (None, 'stream_href', '=', stream_href))
        if stream_record:
            continue

        if stream_type is None:
            stream_comp = urlparse(stream_href)
            if stream_comp.port is not None:
                stream_href, stream_type = _stream_parse(f'{stream_href};')

            if stream_type is None:
                continue

        stream_type = _sanitize_type(stream_type)
        audio_type = 'audio' in stream_type
        can_be_https = 0
        if audio_type:
            can_be_https = _has_https(stream_href)

        log_info += f' {stream_href} ' + '(valid audio)' if audio_type else f'({stream_type})'

        stream_id = uuid4().hex
        stream_values = [stream_id, stream_href, stream_type, can_be_https]
        db.add_one('streams', stream_values)

        radio_updates = 0
        for r in db.find('radios', 'radio_id', (None, 'radio_href', '=', radio_href)):
            radio_id = r[0]
            db.update('radios', ['radio_stream'], [stream_id], ('radio_id', radio_id))
            updated_ids.add(radio_id)
            radio_updates += 1

        if log:
            log_info += f' - {radio_updates} radios updated.'
            print(log_info)
            logging.info(log_info)

    if log:
        logging.info('-- END OF VALIDATION LOG --')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='./wrs.py',
                                     description=f'{PROG_NAME} {PROG_VERSION}. Scrape formatted data from wolradiomap.com',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-s', '--siphon', action='store_true',
                        help=f'gather radios by parsing DOM of urls found in the {CITIES_CSV} file.')
    parser.add_argument('-p', '--print', action='store_true',
                        help='prints usefull information about data integrity and logs.')
    parser.add_argument('-i', '--insert', action='store_true',
                        help=f'insert json files\' data inside the sqlite database.')
    parser.add_argument('-v', '--validate', action='store_true',
                        help='attemps to find valid audio streams for radio links of the database.')
    parser.add_argument('-t', '--test', action='store_true',
                        help='tests commands')
    parser.add_argument('-l', '--locale', type=str, default='fr-FR',
                        help='Language for the address requested during geocoding.')
    parser.add_argument('-L', '--log', action='store_true',
                        help='use a log files in different parts of the program.')
    parser.add_argument('-I', '--index', type=int, default=0,
                        help='an integer to start iteration of some command at a given index.')

    try:
        args = parser.parse_args()
        data_dir = f'{DATA_DIR}/{args.locale}'
        db_file = f'{data_dir}/wrs.db'
        log_file = None
        last_log_index = -1

        if args.log:
            last_log_index = 0
            for log_file in os.listdir(LOGS_DIR):
                log_match = re.search(r'wrs_(\d+).log', log_file)
                if log_match:
                    last_log_index = max(last_log_index, int(log_match.group(1)))
        if args.siphon:
            if args.log:
                log_file = f'{LOGS_DIR}/wrs_{last_log_index+1}.log'
                logging.basicConfig(filename=log_file, level=logging.INFO)
            cmd_siphon(data_dir, args.locale, args.log)
        elif args.print:
            if last_log_index >= 0:
                log_file = f'{LOGS_DIR}/wrs_{last_log_index}.log'
            cmd_print(db_file, data_dir, log_file)
        elif args.insert:
            if args.log:
                log_file = f'{LOGS_DIR}/wrs_db.log'
                logging.basicConfig(filename=log_file, filemode='w', level=logging.INFO)
            cmd_insert(db_file, data_dir, args.index, args.log)
        elif args.test:
            cmd_test(db_file, data_dir)
        elif args.validate:
            if args.log:
                log_file = f'{LOGS_DIR}/wrs_{last_log_index}.log'
                logging.basicConfig(filename=log_file, level=logging.INFO)
            cmd_validate(db_file, args.index, args.log)
        else:
            print('Nothing to do, exiting...')

    except IOError as msg:
        logging.warning(f'Argparse error: {msg}')
