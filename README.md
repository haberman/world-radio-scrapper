## World Radio Scrapper 1.0

```
usage: ./wrs.py [-h] [-s] [-p] [-i] [-v] [-d] [-L] [-I INDEX]

World Radio Scrapper 1.0. Scrape formatted data from wolradiomap.com

optional arguments:
  -h, --help            show this help message and exit
  -s, --siphon          gather radios by parsing DOM of urls found in the
                        ./assets/worldradiomap_cities.csv file. (default:
                        False)
  -p, --print           prints usefull information about data integrity and
                        logs. (default: False)
  -i, --insert          insert json files' data inside the sqlite
                        ./data/wrs.db database. (default: False)
  -v, --validate        attemps to find valid audio streams for radio links of
                        the database. (default: False)
  -d, --debug           command set to only run some custom tests. (default:
                        False)
  -L, --log             use a log files in different parts of the program.
                        (default: False)
  -I INDEX, --index INDEX
                        an integer to start iteration of some command at a
                        given index. (default: 0)
```