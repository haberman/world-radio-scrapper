import os
import re
import sqlite3


class Db:
    def __init__(self, database, queries_init=None):
        self.database = database
        self.queries_init = queries_init

    def _create(self):
        open(self.database, 'a').close()

        if self.queries_init is not None:
            self.conn = sqlite3.connect(self.database)
            self.cursor = self.conn.cursor()
            self.cursor.executescript(';\n'.join(self.queries_init))
            self.close()

    def connect(self):
        if not os.path.isfile(self.database):
            self._create()

        self.conn = sqlite3.connect(self.database, detect_types=sqlite3.PARSE_COLNAMES)
        self.cursor = self.conn.cursor()

    def close(self):
        self.conn.commit()
        self.conn.close()

    def count(self, table):
        self.connect()
        self.cursor.execute(f'SELECT COUNT(*) FROM {table}')

        res = self.cursor.fetchone()
        if res:
            res = res[0]

        self.close()
        return res

    def find(self, table, cols, *argv):
        query = f'SELECT {cols} FROM {table} WHERE'

        values = []
        for logic, row, comp, value in argv:
            if logic is not None:
                query += f' {logic}'
            query += f' {row} {comp} ?'
            values.append(value)
        
        if len(values) == 0:
            return None

        self.connect()
        self.cursor.execute(query, tuple(values))
        res = self.cursor.fetchall()

        self.close()
        return res

    def fetch_one(self, table, row, value):
        query = f'SELECT * FROM {table} WHERE {row}=?'
        self.connect()
        self.cursor.execute(query, (value,))

        res = self.cursor.fetchone()
        self.close()
        return res

    def fetch_join(self, table, ids, join=None):
        ph = '?,' * len(ids)
        query = f'SELECT * FROM {table}'
        if type(join) is tuple:
            query += f' INNER JOIN {join[0]} ON {join[1]} = {join[2]}'
        query += f' WHERE {table[:-1]}_id IN ({ph[:-1]})'

        self.connect()
        self.cursor.execute(query, tuple(ids))

        res = self.cursor.fetchall()
        self.close()
        return res

    def fetch_all(self, table, sort_by=None, order=None, limit=None):
        ''' Returns all rows of given table. '''

        self.connect()

        query = f'SELECT * FROM {table}'
        if sort_by and order:
            query += f'ORDER BY {sort_by} {order}'
        elif sort_by:
            query += f'ORDER BY {sort_by}'
        if limit:
            query += f'{query} LIMIT {limit}'

        self.cursor.execute(query)

        res = self.cursor.fetchall()
        self.close()
        return res

    def fetch_distinct(self, table, rows):
        self.connect()

        query = f'SELECT DISTINCT {",".join(rows)} FROM {table}'
        self.cursor.execute(query)

        res = self.cursor.fetchall()
        self.close()
        return res

    def fetch_group(self, table, groupby):
        self.connect()

        query = f'SELECT * FROM {table} GROUP BY {groupby}'
        self.cursor.execute(query)

        res = self.cursor.fetchall()
        self.close()
        return res

    def add_one(self, table, values):
        ph = '?,' * len(values)
        query = f'INSERT INTO {table} VALUES ({ph[:-1]})'
        
        return self.exec(query, values)

    def add_many(self, table, batch):
        query = f'INSERT INTO {table} VALUES '

        flat_values = []
        for values in batch:
            ph = '?,' * len(values)
            query += f'({ph[:-1]}),'
            flat_values.extend(values)

        return self.exec(query[:-1], flat_values)

    def update(self, table, rows, values, where):
        if len(rows) != len(values):
            return None

        where_col, where_val = where
        query = f'UPDATE {table} SET '
        for row in rows:
            query += f'{row}=?,'
        query = query[:-1]
        query += f' WHERE {where_col}=?'

        values.append(where_val)
        return self.exec(query, values)

    def exec(self, query, values=None):
        self.connect()

        if values and type(values) is not tuple:
            values = tuple(values)

        try:
            if values:
                self.conn.execute(query, values)
            else:
                self.conn.execute(query)
        except sqlite3.IntegrityError as e:
            return f'sqlite error: {e.args[0]}'
        except KeyError as e:
            return f'missing column error: {e.args[0]}'

        finally:
            self.close()

        return True

    def delete_all(self, table):
        self.connect()
        self.cursor.execute(f'DELETE FROM {table}')
        self.close()
